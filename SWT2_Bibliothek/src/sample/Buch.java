package sample;

public class Buch extends Medien
{
    private String isbn;
    private String autohr;

    public Buch(String titel, int anzahl, double preis, String genre, String isbn, String autohr)
    {
        super(titel, anzahl, preis, genre);
        this.isbn = isbn;
        this.autohr = autohr;
    }

    public String toString()
    {
        return "Test";
    }
}
