package sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Kunden<T extends Medien>
{


    private SimpleIntegerProperty kunden_ID;
    private SimpleStringProperty nName;
    private SimpleStringProperty vName;
    private Adresse adresse;
    private Date geburtstag;
    private List<T> momVerleih = new ArrayList<>();
    private List<T> historie;
    private SimpleIntegerProperty anzahlVerliehen;

    public Kunden(int id, String name, String vName, Adresse adresse, Date geburtstag)
    {
        this.kunden_ID = new SimpleIntegerProperty(id);
        this.nName = new SimpleStringProperty(name);
        this.vName = new SimpleStringProperty(vName);
        this.adresse = adresse;
        this.geburtstag = geburtstag;
        this.anzahlVerliehen = new SimpleIntegerProperty(0);
    }

    public void momVerliehn()
    {
        System.out.println(momVerleih.get(0).getTitel());
    }

    public void addMomVerleih(T objekt)
    {
        momVerleih.add(objekt);
    }


    public int getKunden_ID()
    {
        return kunden_ID.get();
    }

    public SimpleIntegerProperty kunden_IDProperty()
    {
        return kunden_ID;
    }

    public void setKunden_ID(int kunden_ID)
    {
        this.kunden_ID.set(kunden_ID);
    }


    public String getnName()
    {
        return nName.get();
    }

    public SimpleStringProperty nNameProperty()
    {
        return nName;
    }

    public void setnName(String nName)
    {
        this.nName.set(nName);
    }

    public String getvName()
    {
        return vName.get();
    }

    public SimpleStringProperty vNameProperty()
    {
        return vName;
    }

    public void setvName(String vName)
    {
        this.vName.set(vName);
    }

    public Adresse getAdresse()
    {
        return adresse;
    }

    public void setAdresse(Adresse adresse)
    {
        this.adresse = adresse;
    }

    public Date getGeburtstag()
    {
        return geburtstag;
    }

    public void setGeburtstag(Date geburtstag)
    {
        this.geburtstag = geburtstag;
    }

    public List<T> getMomVerleih()
    {
        return momVerleih;
    }

    public void setMomVerleih(List<T> momVerleih)
    {
        this.momVerleih = momVerleih;
    }

    public List<T> getHistorie()
    {
        return historie;
    }

    public void setHistorie(List<T> historie)
    {
        this.historie = historie;
    }

    public Integer getanzahlVerliehen()
    {
        return anzahlVerliehen.get();
    }

    public SimpleIntegerProperty anzahlVerliehenProperty()
    {
        return anzahlVerliehen;
    }

    public void setAnzahlVerliehen(int anzahlVerliehen)
    {
        this.anzahlVerliehen.set(anzahlVerliehen);
    }

}
