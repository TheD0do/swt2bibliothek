package sample;

public class Adresse
{


    private String strasse;
    private String hsnr;
    private String ort;
    private String plz;

    public Adresse(String strasse, String hsnr, String ort, String plz)
    {
        this.strasse = strasse;
        this.hsnr = hsnr;
        this.ort = ort;
        this.plz = plz;
    }


    public String getStrasse()
    {
        return strasse;
    }

    public void setStrasse(String strasse)
    {
        this.strasse = strasse;
    }

    public String getHsnr()
    {
        return hsnr;
    }

    public void setHsnr(String hsnr)
    {
        this.hsnr = hsnr;
    }

    public String getOrt()
    {
        return ort;
    }

    public void setOrt(String ort)
    {
        this.ort = ort;
    }

    public String getPlz()
    {
        return plz;
    }

    public void setPlz(String plz)
    {
        this.plz = plz;
    }



    public String toString()
    {
        return "Straße: " + strasse +
                "\nHausnummer: " + hsnr +
                "\nOrt: " + ort +
                "\nPostleitzahl: " + plz;
    }
}
