package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Date;
import java.time.LocalDate;

public class Main extends Application
{

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        System.out.println("Hi");
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/hauptmenue.fxml"));
        primaryStage.setTitle("SWT2_Bibliothek");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args)
    {
        launch(args);
    }
}
