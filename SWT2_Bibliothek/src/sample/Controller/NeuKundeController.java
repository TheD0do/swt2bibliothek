package sample.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class NeuKundeController implements Initializable
{
    @FXML
    private Button cmdKundeHinzufuegen, cmdAbbrechen;
    @FXML
    private TextField txtVornameErstellen,
            txtNachnameErstellen,
            txtStrassenName,
            txtHausnummer,
            txtPLZ,
            txtOrt;
    @FXML
    private DatePicker dateKalender;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {

    }

    public void cmdErstellenKlick(ActionEvent event) throws IOException
    {
        Connection conn = new DatenbankDaten().verbindeDB();
        PreparedStatement pst = null;


        dbFuegeOrtEin(txtPLZ.getText(), txtOrt.getText(), pst, conn);
        String adresse_id = dbFuegeAdresseEin(txtStrassenName.getText(),
                txtHausnummer.getText(),
                txtPLZ.getText(),
                pst,
                conn);
        dbFuegeKundeEin(txtVornameErstellen.getText(),
                txtNachnameErstellen.getText(),
                Date.valueOf(dateKalender.getValue()),
                adresse_id,
                pst,
                conn);
        cmdAbbrechenKlick(event);
    }

    public void dbFuegeOrtEin(String plz, String ort, PreparedStatement pst, Connection conn)
    {
        String sqlOrt = "INSERT IGNORE INTO ort (plz,ort) VALUES (?,?)";
        try
        {
            pst = conn.prepareStatement(sqlOrt);
            pst.setString(1, plz);
            pst.setString(2, ort);
            pst.execute();
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

    }

    public String dbFuegeAdresseEin(String strasse, String hausnummer,
                                    String plz, PreparedStatement pst, Connection conn)
    {
        String sqlAdresse = "INSERT INTO testdatenbank.adresse (strasse,hausnummer,plz) \n" +
                "VALUES (?,?,?) ON DUPLICATE KEY UPDATE strasse = ?;";
        String sqlIndex = "SELECT idadresse FROM adresse WHERE strasse = ? AND hausnummer = ? AND plz = ?";
        String test ="";
        try
        {
            pst = conn.prepareStatement(sqlAdresse);
            pst.setString(1, strasse);
            pst.setString(2, hausnummer);
            pst.setString(3, plz);
            pst.setString(4, strasse);
            pst.execute();

            pst = conn.prepareStatement(sqlIndex);
            pst.setString(1, strasse);
            pst.setString(2, hausnummer);
            pst.setString(3, plz);
            System.out.println("TEST0.5");
            ResultSet rs = pst.executeQuery();

            if (rs.next())
                 test = rs.getString(1);
            return test;
        }
        catch (SQLException throwables)
        {

            try
            {
                pst = conn.prepareStatement(sqlIndex);
                pst.setString(1, strasse);
                pst.setString(2, hausnummer);
                pst.setString(3, plz);
                ResultSet rs = pst.executeQuery();

                if (rs.next())
                    test = rs.getString(1);
                return test;
            }
            catch (SQLException throwables2)
            {
            }
        }
        return "0";
    }

    public void dbFuegeKundeEin(String vName, String nName, Date bDay, String adresse, PreparedStatement pst, Connection conn)
    {
        String sqlKunde = "INSERT INTO kunden (vName,nName,bDay,adresseid) VALUES (?,?,?,?)";
        try
        {
            pst = conn.prepareStatement(sqlKunde);
            pst.setString(1, vName);
            pst.setString(2, nName);
            pst.setString(3, String.valueOf(bDay));
            pst.setString(4, adresse);
            pst.execute();
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

    }


    public void cmdAbbrechenKlick(ActionEvent event) throws IOException
    {
        Parent hauptmenueParent = FXMLLoader.load(getClass().getResource("../GUI/kunden.fxml"));
        Scene hauptmenueSzene = new Scene(hauptmenueParent);

        Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();

        fenster.setScene(hauptmenueSzene);
        fenster.show();
    }


}
