package sample.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller
{
    @FXML
    private AnchorPane paneHauptSzene;
    private Stage stage;
    private Scene scene;
    private Parent root;

    public void cmdWechselZuKunden(ActionEvent event)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Look, an Information Dialog");
        alert.setContentText("I have a great message for you!");

        try
        {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/kunden.fxml"));
            root = loader.load();

            KundenController kController = loader.getController();

            //root = FXMLLoader.load(getClass().getResource("../GUI/kunden.fxml"));
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e)
        {
            e.printStackTrace();

        }
    }

    public void cmdLagerKlick(ActionEvent event)
    {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Look, an Information Dialog");
        alert.setContentText("I have a great message for you!");

        alert.showAndWait();

    }

    public void wechsleZuNeuKunde(ActionEvent event) throws IOException
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/neuKunde.fxml"));
            Parent neuKundeParent = loader.load();
            Scene neuKundeSzene = new Scene(neuKundeParent);

            NeuKundeController controller = loader.getController();


            Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();
            fenster.setScene(neuKundeSzene);
            fenster.show();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void logOutKlick(ActionEvent event)
    {
        stage = (Stage) paneHauptSzene.getScene().getWindow();
        stage.close();
    }
}
