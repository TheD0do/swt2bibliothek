package sample.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Kunden;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class KundenDetailController implements Initializable
{
    private Kunden ausgewaehlterKunde;

    @FXML
    private Label lblAnzeigeName;
    @FXML
    private Label lblProfilDaten;
    @FXML
    private Button cmdProfilZurueck;
    @FXML
    private Button cmdRueckgeben;
    @FXML
    private Button cmdVerleihen;


    public void initialisiereDaten(Kunden kunde)
    {
        this.ausgewaehlterKunde = kunde;
        lblAnzeigeName.setText(kunde.getKunden_ID() + " " + kunde.getvName() + " " + kunde.getnName());
        lblProfilDaten.setText("Geburtstag: " + kunde.getGeburtstag()+
                "\n" + kunde.getAdresse().toString()+
                "\nAnzahl verliehen: " + kunde.getanzahlVerliehen());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {

    }

    public void cmdZurueckKlick(ActionEvent event) throws IOException
    {
        Parent hauptmenueParent = FXMLLoader.load(getClass().getResource("../GUI/kunden.fxml"));
        Scene hauptmenueSzene = new Scene(hauptmenueParent);

        Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();

        fenster.setScene(hauptmenueSzene);
        fenster.show();
    }
}
