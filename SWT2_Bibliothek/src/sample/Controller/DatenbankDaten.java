package sample.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Adresse;
import sample.Kunden;

import java.sql.*;

public class DatenbankDaten
{
    private String username;
    private String pw;

    private Connection conn;

    public Connection verbindeDB()
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");

            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/testdatenbank", "root", "R00tToor");
            return conn;

        }
        catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public ObservableList<Kunden> getDatenKunden()
    {
        Connection conn = verbindeDB();
        ObservableList<Kunden> liste = FXCollections.observableArrayList();
        try
        {
            PreparedStatement ps = conn.prepareStatement("SELECT kunde_id,\n" +
                    "vName,\n" +
                    "nName,\n" +
                    "bDay,\n" +
                    "strasse,\n" +
                    "hausnummer,\n" +
                    "adresse.plz,\n" +
                    "ort\n" +
                    "FROM testdatenbank.kunden as kunde\n" +
                    "LEFT JOIN testdatenbank.adresse AS adresse\n" +
                    "ON kunde.adresseid = adresse.idadresse\n" +
                    "LEFT JOIN testdatenbank.ort AS ort\n" +
                    "ON adresse.plz = ort.plz");
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                liste.add(
                        new Kunden(Integer.parseInt(rs.getString("kunde_id")),
                                rs.getString("nName"),
                                rs.getString("vName"),
                                new Adresse(rs.getString("strasse"),
                                        rs.getString("hausnummer"),
                                        rs.getString("ort"),
                                        rs.getString("plz")),
                                rs.getDate("bDay"))
                );
            }
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }


        return liste;
    }
}
