package sample.Controller;

import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.Kunden;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;


public class KundenController implements Initializable
{

    @FXML
    TextField txtSuchleiste;
    @FXML
    Button cmdKZurueck, cmdKAnzeigen, cmdNeuKunde, cmdKundenEntfernen;
    @FXML
    TableView<Kunden> tblKTabelle;
    @FXML
    TableColumn<Kunden, Integer> rowKID;
    @FXML
    TableColumn<Kunden, String> rowVName;
    @FXML
    TableColumn<Kunden, String> rowNName;
    @FXML
    TableColumn<Kunden, Date> rowBDay;
    @FXML
    TableColumn<Kunden, Integer> rowAnzVerliehen;

    private ObservableList<Kunden> liste;


    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement pst = null;
    private DatenbankDaten verbindung = new DatenbankDaten();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        rowKID.setCellValueFactory(new PropertyValueFactory<Kunden, Integer>("kunden_ID"));
        rowVName.setCellValueFactory(new PropertyValueFactory<Kunden, String>("vName"));
        rowNName.setCellValueFactory(new PropertyValueFactory<Kunden, String>("nName"));
        rowBDay.setCellValueFactory(new PropertyValueFactory<Kunden, Date>("geburtstag"));
        rowAnzVerliehen.setCellValueFactory(new PropertyValueFactory<Kunden, Integer>("anzahlVerliehen"));


        liste = verbindung.getDatenKunden();
        // Eine Gefilterte Liste wird erstellt, diese hilft dabei nach Elementen zu suchen
        FilteredList<Kunden> filteredListe = new FilteredList<>(liste, p -> true);
        txtSuchleiste.textProperty().addListener((observable, oldValue, newValue) ->
        {
            filteredListe.setPredicate(kunden ->
            {
                if (newValue == null || newValue.isEmpty())
                    return true;
                String lowerCaseFilter = newValue.toLowerCase();

                if (kunden.getvName().toLowerCase().contains(lowerCaseFilter))
                    return true;
                else if (kunden.getnName().toLowerCase().contains(lowerCaseFilter))
                    return true;
                return false;
            });
        });

        SortedList<Kunden> sortierteListe = new SortedList<>(filteredListe);

        sortierteListe.comparatorProperty().bind(tblKTabelle.comparatorProperty());


        //tblKTabelle.getColumns().setAll(rowVName, rowNName, rowBDay, rowAnzVerliehen);
        tblKTabelle.setItems(sortierteListe);
    }


    public void cmdZurueckKlick(ActionEvent event) throws IOException
    {
        Parent hauptmenueParent = FXMLLoader.load(getClass().getResource("../GUI/hauptmenue.fxml"));
        Scene hauptmenueSzene = new Scene(hauptmenueParent);

        Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();

        fenster.setScene(hauptmenueSzene);
        fenster.show();
    }

    public void kundeAnzeigen(ActionEvent event) throws IOException
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/kProfil.fxml"));
            Parent kundenProfilParent = loader.load();
            Scene kundenProfilSzene = new Scene(kundenProfilParent);

            KundenDetailController controller = loader.getController();
            controller.initialisiereDaten(tblKTabelle.getSelectionModel().getSelectedItem());


            Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();
            fenster.setScene(kundenProfilSzene);
            fenster.show();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void wechsleZuNeuKunde(ActionEvent event) throws IOException
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../GUI/neuKunde.fxml"));
            Parent neuKundeParent = loader.load();
            Scene neuKundeSzene = new Scene(neuKundeParent);

            NeuKundeController controller = loader.getController();


            Stage fenster = (Stage) ((Node) event.getSource()).getScene().getWindow();
            fenster.setScene(neuKundeSzene);
            fenster.show();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void kundenEntfernenKlick(ActionEvent event)
    {
        Connection conn = new DatenbankDaten().verbindeDB();
        try
        {
            PreparedStatement pst = conn.prepareStatement(
                    "DELETE FROM kunden WHERE kunde_id = " +
                            tblKTabelle.getSelectionModel().getSelectedItem().getKunden_ID());
            pst.execute();
            Controller controller = new Controller();
            controller.cmdWechselZuKunden(event);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }
}
