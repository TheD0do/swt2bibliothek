package sample;

import java.util.Date;

public abstract class Medien
{
    public double getPreis()
    {
        return preis;
    }

    public void setPreis(double preis)
    {
        this.preis = preis;
    }

    public int getVerfuegbar()
    {
        return verfuegbar;
    }

    public void setVerfuegbar(int verfuegbar)
    {
        this.verfuegbar = verfuegbar;
    }

    public String getTitel()
    {
        return titel;
    }

    public void setTitel(String titel)
    {
        this.titel = titel;
    }

    public Date getVerleihVon()
    {
        return verleihVon;
    }

    public void setVerleihVon(Date verleihVon)
    {
        this.verleihVon = verleihVon;
    }

    public Date getVerleihBis()
    {
        return verleihBis;
    }

    public void setVerleihBis(Date verleihBis)
    {
        this.verleihBis = verleihBis;
    }

    public String getGenre()
    {
        return genre;
    }

    public void setGenre(String genre)
    {
        this.genre = genre;
    }

    private double preis;
    private int anzahl;
    private int verfuegbar;
    private String titel;
    private Date verleihVon;
    private Date verleihBis;
    private String genre;

    public Medien(String titel, int anzahl, double preis, String genre)
    {
        this.preis = preis;
        this.anzahl = anzahl;
        this.titel = titel;
        this.genre = genre;
    }


    public void verleihen()
    {

    }

    public boolean istVerfuegbar()
    {
        return verfuegbar >0?true:false;
    }
}
